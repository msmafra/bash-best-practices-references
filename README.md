# Bash, Bash Best Practices and References
## Bash, Zsh, Fish, Scripts

## Links to Bash Best Practices References

- [Bash Script Pitfalls!!!](https://mywiki.wooledge.org/BashPitfalls) **I do most of those. Am I wrong? :) **
- [Some Bash coding conventions and good practices](https://github.com/icy/bash-coding-style) - **good to update/improve scripts from now on**
- [Templates to write better Bash scripts](https://bash3boilerplate.sh/)
- [BashPitfalls](https://mywiki.wooledge.org/BashPitfalls#echo_.3C.3CEOF)
- [Beginner Mistakes](https://wiki.bash-hackers.org/scripting/newbie_traps)
- [The Bash Hackers Wiki](https://wiki.bash-hackers.org/)
- [Cheat sheets](https://bertvv.github.io/cheat-sheets/)
- [Good practices for writing shell scripts](http://www.yoone.eu/articles/2-good-practices-for-writing-shell-scripts.html) - **no https!!**
- [Best Practices for Writing Bash Scripts](https://kvz.io/blog/2013/11/21/bash-best-practices/)
- [Shell Scripting - Best Practices](https://fahdshariff.blogspot.com/2013/10/shell-scripting-best-practices.html)
- [What you need to know about bash functions](http://blog.joncairns.com/2013/08/what-you-need-to-know-about-bash-functions/) - **no https!!**
- [Licenses](https://choosealicense.com/licenses/)
- [Shell Scripting Tutorial](https://www.shellscript.sh/)
- [BashingLinux Conventions](https://bashinglinux.wordpress.com/2009/08/01/conventions/)
- [Obsolete and deprecated syntax](https://wiki.bash-hackers.org/scripting/obsolete)
- [Best Practices for Bash Scripts](https://medium.com/better-programming/best-practices-for-bash-scripts-17229889774d)
- [TechMint shell scripting - Page 1](https://www.tecmint.com/tag/shell-scripting/)
- [TechMint shell scripting - Page 2](https://www.tecmint.com/tag/shell-scripting/page/2/)

### FISH
- [Oh My Fish for fish shell](https://github.com/oh-my-fish/oh-my-fish)
- [Fedora Magazine's Fish – A Friendly Interactive Shell](https://fedoramagazine.org/fish-a-friendly-interactive-shell/)

### ZSH
- [ZSH](https://www.zsh.org/)
- [Zsh builin commands](http://zsh.sourceforge.net/Doc/Release/Shell-Builtin-Commands.html) - **no https!!**
- [Oh My Zsh framework for Zsh shell:](https://github.com/ohmyzsh/ohmyzsh)
- [OhMyZsh history date formatting](https://stackoverflow.com/questions/37971768/oh-my-zsh-history-date-formatting#37978767)
- [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
- [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)
- [zsh-history-substring-search](https://github.com/zsh-users/zsh-history-substring-search)
- [FedoraMagazine's](https://fedoramagazine.org/tuning-your-bash-or-zsh-shell-in-workstation-and-silverblue/)
- [Xterm 256 color chart](https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg)

## Other helpful BASH references

- [Bash tips: Colors and formatting (ANSI/VT100 Control sequences)](https://misc.flogisoft.com/bash/tip_colors_and_formatting)
- [Bert Van Vreckem's](https://github.com/bertvv/dotfiles)
- [Advanced Bash-Scripting Guide](https://www.tldp.org/LDP/abs/html/)
- [Small code snippets](https://wiki.bash-hackers.org/snipplets/start)
- [Scripting: If Comparison Operators In Bash](https://blog.100tb.com/scripting-if-comparison-operators-in-bash)
- [Prompt for sudo password and programmatically elevate privilege in bash script?](https://unix.stackexchange.com/questions/28791/prompt-for-sudo-password-and-programmatically-elevate-privilege-in-bash-script)
- [/usr/bin vs /usr/local/bin on Linux](https://unix.stackexchange.com/questions/8656/usr-bin-vs-usr-local-bin-on-linux)
- [Bash Conditional Expressions](https://www.gnu.org/software/bash/manual/html_node/Bash-Conditional-Expressions.html)
- [Bash Conditional Expressions (Bash Reference Manual)](https://www.gnu.org/software/bash/manual/html_node/Bash-Conditional-Expressions.html#Bash-Conditional-Expressions)
- [Argbash documentation](https://argbash.readthedocs.io/en/stable/index.html)
- [Shell Scripting Tutorial](https://www.youtube.com/watch?v=hwrnmQumtPw)
- [Shell Ninja: Mastering the Art of Shell Scripting | Roland Huß](https://www.youtube.com/watch?v=1mt2-LbKuvY)
- [Introduction to Advanced Bash Usage - James Pannacciulli @ OSCON 2014](https://www.youtube.com/watch?v=uqHjc7hlqd0)
- [Shell Style Guide](https://google.github.io/styleguide/shell.xml?showone=SUID/SGID#SUID/SGID)
- [Shell Parameter Expansion](https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html)
- [progrium/bashstyle](https://github.com/progrium/bashstyle)
- [Bash Functions](https://linuxize.com/post/bash-functions/)
- [alexanderepstein Bash-Snippets](https://github.com/alexanderepstein/Bash-Snippets)
- [TutorialKart's Bash tutorials](https://www.tutorialkart.com/bash-shell-scripting/bash-tutorial/)
- [Bash exit codes](https://www.tldp.org/LDP/abs/html/exitcodes.html)
- [Bash it out Book](https://opensource.com/article/20/4/bash-it-out-book)
- [Bash Conditional Expressions](https://www.gnu.org/software/bash/manual/html_node/Bash-Conditional-Expressions.html#Bash-Conditional-Expressions)
- [Bash Select Command &#8211; Linux Hint](https://linuxhint.com/bash_select_command/)
- [A sysadmin's guide to Bash scripting | Opensource.com](https://opensource.com/downloads/bash-scripting-ebook)
- [Chezmoi backup dot files](https://github.com/twpayne/chezmoi)
- [GitHub - SoptikHa2/desed: Debugger for Sed: demystify and debug your sed scripts, from comfort of your terminal.](https://github.com/SoptikHa2/desed)
- [Bash Find out the exit codes of all piped commands](https://www.cyberciti.biz/faq/unix-linux-bash-find-out-the-exit-codes-of-all-piped-commands/)
- [Vim Awesome](https://vimawesome.com/)
- [Comparison Operators](https://www.tldp.org/LDP/abs/html/comparison-ops.html)
- [Using Bash printf Command for Printing Formatted Outputs](https://linuxhandbook.com/bash-printf/)

> Bash non directly related links are now on:
  [Great Tools and Guides](https://gitlab.com/msmafra/great-tools-and-guides)
